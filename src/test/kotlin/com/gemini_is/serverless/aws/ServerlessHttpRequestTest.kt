package com.gemini_is.serverless.aws

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.*
import org.intellij.lang.annotations.Language
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class ServerlessHttpRequestTest {
  @Test
  fun getParam_gets_booleans_correctly() {
    // Check true value
    @Language("json")
    var requestBody = """{"test":true}"""
    var request = ServerlessHttpRequest(
      APIGatewayProxyRequestEvent()
        .withBody(requestBody)
        .withHttpMethod("GET")
        .withPath("/")
    )
    var result = request.getParam<Boolean>("test")
    assertTrue(result!!)

    // Check false value
    @Language("json")
    requestBody = """{"test":false}"""
    request = ServerlessHttpRequest(
      APIGatewayProxyRequestEvent()
        .withBody(requestBody)
        .withHttpMethod("GET")
        .withPath("/")
    )
    result = request.getParam<Boolean>("test")
    assertFalse(result!!)
  }

  @Test
  fun getParam_gets_lists_correctly() {
    @Language("json")
    val requestBody = """{"test":["foo","bar","baz"]}"""
    val request = ServerlessHttpRequest(
      APIGatewayProxyRequestEvent()
        .withBody(requestBody)
        .withHttpMethod("GET")
        .withPath("/")
    )
    val result = request.getParam<List<String>>("test")
    assertThat(result!!.size, `is`(3))
  }

  @Test
  fun getParam_gets_maps_correctly() {
    @Language("json")
    val requestBody = """{"test":{"foo":"bar","baz":["zed"]}}"""
    val request = ServerlessHttpRequest(
      APIGatewayProxyRequestEvent()
        .withBody(requestBody)
        .withHttpMethod("GET")
        .withPath("/")
    )
    val result = request.getParam<Map<String, Any>>("test")
    assertThat(result!!["baz"], `is`(listOf("zed")))
  }

  @Test
  fun getParam_gets_ints_correctly() {
    @Language("json")
    val requestBody = """{"test":123}"""
    val request = ServerlessHttpRequest(
      APIGatewayProxyRequestEvent()
        .withBody(requestBody)
        .withHttpMethod("GET")
        .withPath("/")
    )
    val result = request.getParam<Int>("test")
    assertThat(result, `is`(123))
  }

  @Test
  fun getParam_gets_doubles_correctly() {
    @Language("json")
    val requestBody = """{"test":50.184}"""
    val request = ServerlessHttpRequest(
      APIGatewayProxyRequestEvent()
        .withBody(requestBody)
        .withHttpMethod("GET")
        .withPath("/")
    )
    val result = request.getParam<Double>("test")
    assertThat(result, `is`(50.184))
  }

  @Test
  fun getParam_gets_strings_as_numbers_correctly() {
    @Language("json")
    var requestBody = """{"test":"50"}"""
    var request = ServerlessHttpRequest(
      APIGatewayProxyRequestEvent()
        .withBody(requestBody)
        .withHttpMethod("GET")
        .withPath("/")
    )
    var result: Number = request.getParam<Int>("test") as Number
    assertThat(result, `is`(50))

    @Language("json")
    requestBody = """{"test":"50.184"}"""
    request = ServerlessHttpRequest(
      APIGatewayProxyRequestEvent()
        .withBody(requestBody)
        .withHttpMethod("GET")
        .withPath("/")
    )
    result = request.getParam<Double>("test") as Number
    assertThat(result, `is`(50.184))
  }
}
