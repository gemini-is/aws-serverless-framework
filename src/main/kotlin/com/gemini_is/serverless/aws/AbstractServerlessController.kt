package com.gemini_is.serverless.aws

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent
import com.gemini_is.utils.SystemUtils

abstract class AbstractServerlessController(event: APIGatewayProxyRequestEvent, var context: Context) {
  var request: ServerlessHttpRequest

  init {
    request = ServerlessHttpRequest(event)
    SystemUtils.loadResourcesFile()
  }

  abstract fun route(): APIGatewayProxyResponseEvent?
}
