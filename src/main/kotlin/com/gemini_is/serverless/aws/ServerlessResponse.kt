package com.gemini_is.serverless.aws

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent
import com.gemini_is.utils.SystemUtils
import com.gemini_is.utils.Utils
import org.apache.http.entity.ContentType
import java.io.Serializable
import java.util.*

open class ServerlessResponse : APIGatewayProxyResponseEvent, Serializable {
  private val headers: MutableMap<String, String> = TreeMap(java.lang.String.CASE_INSENSITIVE_ORDER)

  constructor() {
    headers["Content-Type"] = ContentType.TEXT_PLAIN.mimeType
    super.setHeaders(headers)
  }

  constructor(request: ServerlessHttpRequest) : super() {
    val rawValidOrigins = SystemUtils.getVariable("VALID_ORIGINS")
    val incomingOrigin = request.getHeader("origin")
    if (Utils.checkParams(rawValidOrigins, incomingOrigin)) {
      val validOrigins = listOf(*rawValidOrigins!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }
        .toTypedArray())
      val origin = request.getHeader("origin") ?: "*"
      if (validOrigins.contains(incomingOrigin)) {
        headers["Access-Control-Allow-Origin"] = origin
        headers["Access-Control-Allow-Headers"] = origin
        headers["Access-Control-Allow-Credentials"] = "true"
        headers["Access-Control-Allow-Methods"] = "GET, HEAD, POST, PUT, PATCH, DELETE"
        super.setHeaders(headers)
      }
    }
  }

  fun withHeader(key: String, value: String): ServerlessResponse {
    headers[key] = value
    super.setHeaders(headers)
    return this
  }

  fun withContentType(mimeType: ContentType): ServerlessResponse {
    headers.replace("Content-Type", mimeType.mimeType)
    super.setHeaders(headers)
    return this
  }

  override fun withStatusCode(statusCode: Int): ServerlessResponse {
    return super.withStatusCode(statusCode) as ServerlessResponse
  }

  override fun withIsBase64Encoded(isBase64Encoded: Boolean): ServerlessResponse {
    return super.withIsBase64Encoded(isBase64Encoded) as ServerlessResponse
  }

  override fun withHeaders(headers: Map<String, String>): ServerlessResponse {
    return super.withHeaders(headers) as ServerlessResponse
  }

  companion object {
    private const val serialVersionUID = 77268209657100470L
  }
}
