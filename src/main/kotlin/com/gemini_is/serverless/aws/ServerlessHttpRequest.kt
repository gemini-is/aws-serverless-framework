package com.gemini_is.serverless.aws

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import com.gemini_is.utils.Utils
import com.google.common.collect.ArrayListMultimap
import com.google.gson.Gson
import org.apache.commons.fileupload.MultipartStream
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.net.URLDecoder
import java.nio.charset.StandardCharsets
import java.util.*

@Suppress("CanBeParameter", "MemberVisibilityCanBePrivate", "unused")
class ServerlessHttpRequest(val event: APIGatewayProxyRequestEvent) {
  private val headers: MutableMap<String, String> = TreeMap(java.lang.String.CASE_INSENSITIVE_ORDER)
  var params: ArrayListMultimap<String, Any> = ArrayListMultimap.create()
  val path: String = event.path
  val method: String = event.httpMethod

  init {
    initFromAws(event)
  }

  private fun initFromAws(event: APIGatewayProxyRequestEvent) {
    // Map query params
    if (event.queryStringParameters.isNullOrEmpty()
        .not()
    ) event.queryStringParameters.forEach { (k: String?, v: String) ->
      params.put(k, v)
    }
    // Map path params
    if (event.pathParameters.isNullOrEmpty().not()) event.pathParameters.forEach { (k: String?, v: String) ->
      params.put(k, v)
    }
    if (event.headers.isNullOrEmpty().not()) event.headers.forEach { (k: String, v: String) ->
      headers[k] = v
    }
    var body = event.body
    if (Utils.checkParams(body)) {
      // Checks if the body is base64 encoded
      try {
        // If the body is encoded then it decodes it
        val decoder = Base64.getDecoder()
        body = String(decoder.decode(body))
      } catch (ignore: IllegalArgumentException) {
        // The body wasn't base64 encoded.
      }
      when (body[0]) {
        '-' -> setParamsFromMultipartRequest(body)
        '{', '[' -> setParamsFromJSONRequest(body)
        else -> setParamsFromUrlEncodedForm(body)
      }
    }
  }

  private fun setParamsFromUrlEncodedForm(body: String) {
    for (s in body.split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
      params.put(
        URLDecoder.decode(s.substring(0, s.indexOf("=")), StandardCharsets.UTF_8),
        URLDecoder.decode(s.substring(s.indexOf("=") + 1), StandardCharsets.UTF_8)
      )
    }
  }

  private fun setParamsFromJSONRequest(body: String) {
    Gson().fromJson<HashMap<String, Any>>(body, HashMap::class.java)
      .forEach { (key: String?, value: Any) -> params.put(key, value) }
  }

  private fun setParamsFromMultipartRequest(body: String) {
    val bI = body.toByteArray()
    val boundary = headers["Content-Type"]!!.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
    val multipartStream = MultipartStream(
      ByteArrayInputStream(bI),
      boundary.toByteArray(),
      bI.size,
      null
    )
    val paramsAsByteArray = ArrayListMultimap.create<String, ByteArray>()
    try {
      ByteArrayOutputStream().use { out ->
        var nextPart = multipartStream.skipPreamble()

        //Loop through each segment
        while (nextPart) {
          val header = multipartStream.readHeaders()

          //Write out the file to our ByteArrayOutputStream
          multipartStream.readBodyData(out)
          paramsAsByteArray.put(
            header.replace("\"", "")
              .replace("\r\n", "")
              .replace("; filename", "")
              .split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1],
            out.toByteArray()
          )
          out.reset()

          //Get the next part, if any
          nextPart = multipartStream.readBoundary()
        }
        paramsAsByteArray.forEach(params::put)
      }
    } catch (error: Exception) {
      throw RuntimeException(error)
    }
  }

  fun getHeader(headerName: String): String? {
    return headers[headerName]
  }

  inline fun <reified T> getParam(paramName: String): T? {
    val param = params[paramName] ?: return null

    if (param.isEmpty()) return null

    return when (T::class) {
      Boolean::class -> param[0].toString().toBoolean() as T
      Int::class -> param[0].toString().toDouble().toInt() as T
      Double::class -> param[0].toString().toDouble() as T
      List::class -> Gson().fromJson(param[0].toString(), T::class.java)
      Map::class -> Gson().fromJson(param[0].toString(), T::class.java)
      else -> param[0] as T
    }
  }

  @Deprecated("getParam: String is now generic and should be used as such (getParam<T>)")
  fun getParam(paramName: String): String? {
    val param = params[paramName] ?: return null
    return if (param.isNotEmpty()) param[0] as? String else null
  }

  fun getParamAsList(paramName: String?): List<String>? {
    val param = params[paramName]
    return if (param != null && param.isNotEmpty()) {
      param.map { (it as String) }
    } else null
  }

  fun getParamAsByteArray(paramName: String?): ByteArray? {
    val param = params[paramName]
    return if (param != null && param.isNotEmpty()) (param[0] as String).toByteArray() else null
  }

  fun getParamAsByteArrayList(paramName: String?): List<ByteArray> {
    val param = params[paramName] ?: throw NullPointerException("[param.$paramName] is null")

    return when {
      param[0] is String -> {
        param.map { (it as String).toByteArray() }
      }
      else -> {
        return listOf(Gson().toJson(param).toByteArray())
      }
    }
  }
}
