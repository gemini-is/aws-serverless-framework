package com.gemini_is.serverless.aws.responses

import com.gemini_is.serverless.aws.ServerlessHttpRequest
import com.gemini_is.serverless.aws.ServerlessResponse

class Conflict : ServerlessResponse {
  constructor() : super() {
    super.setStatusCode(409)
    super.setBody("Conflict")
  }

  constructor(request: ServerlessHttpRequest?) : super(request!!) {
    super.setStatusCode(409)
    super.setBody("Conflict")
  }
}
