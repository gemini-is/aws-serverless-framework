package com.gemini_is.serverless.aws.responses

import com.gemini_is.serverless.aws.ServerlessHttpRequest
import com.gemini_is.serverless.aws.ServerlessResponse

class BadRequest : ServerlessResponse {
  constructor() : super() {
    super.setStatusCode(400)
    super.setBody("Bad Request")
  }

  constructor(request: ServerlessHttpRequest?) : super(request!!) {
    super.setStatusCode(400)
    super.setBody("Bad Request")
  }
}
