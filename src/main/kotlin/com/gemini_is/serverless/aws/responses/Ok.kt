package com.gemini_is.serverless.aws.responses

import com.gemini_is.serverless.aws.ServerlessHttpRequest
import com.gemini_is.serverless.aws.ServerlessResponse

class Ok : ServerlessResponse {
  constructor() : super() {
    super.setStatusCode(200)
    super.setBody("OK")
  }

  constructor(request: ServerlessHttpRequest?) : super(request!!) {
    super.setStatusCode(200)
    super.setBody("OK")
  }
}
