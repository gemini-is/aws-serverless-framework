package com.gemini_is.serverless.aws.responses

import com.gemini_is.serverless.aws.ServerlessHttpRequest
import com.gemini_is.serverless.aws.ServerlessResponse

class Unauthorized : ServerlessResponse {
  constructor() : super() {
    super.setStatusCode(401)
    super.setBody("Not Authorized")
  }

  constructor(request: ServerlessHttpRequest?) : super(request!!) {
    super.setStatusCode(401)
    super.setBody("Not Authorized")
  }
}
