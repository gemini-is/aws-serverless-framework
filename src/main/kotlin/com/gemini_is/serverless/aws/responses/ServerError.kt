package com.gemini_is.serverless.aws.responses

import com.gemini_is.serverless.aws.ServerlessHttpRequest
import com.gemini_is.serverless.aws.ServerlessResponse

class ServerError : ServerlessResponse {
  constructor() : super() {
    super.setStatusCode(500)
    super.setBody("Server Error")
  }

  constructor(request: ServerlessHttpRequest?) : super(request!!) {
    super.setStatusCode(500)
    super.setBody("Server Error")
  }
}
