package com.gemini_is.serverless.aws.responses

import com.gemini_is.serverless.aws.ServerlessHttpRequest
import com.gemini_is.serverless.aws.ServerlessResponse

class MethodNotAllowed : ServerlessResponse {
  constructor() : super() {
    super.setStatusCode(405)
    super.setBody("Method Not Allowed")
  }

  constructor(request: ServerlessHttpRequest?) : super(request!!) {
    super.setStatusCode(405)
    super.setBody("Method Not Allowed")
  }
}
