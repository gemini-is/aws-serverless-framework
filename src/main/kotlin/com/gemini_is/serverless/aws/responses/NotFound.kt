package com.gemini_is.serverless.aws.responses

import com.gemini_is.serverless.aws.ServerlessHttpRequest
import com.gemini_is.serverless.aws.ServerlessResponse

class NotFound : ServerlessResponse {
  constructor() : super() {
    super.setStatusCode(404)
    super.setBody("Not Found")
  }

  constructor(request: ServerlessHttpRequest?) : super(request!!) {
    super.setStatusCode(404)
    super.setBody("Not Found")
  }
}
