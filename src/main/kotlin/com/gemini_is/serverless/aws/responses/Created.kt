package com.gemini_is.serverless.aws.responses

import com.gemini_is.serverless.aws.ServerlessHttpRequest
import com.gemini_is.serverless.aws.ServerlessResponse

class Created : ServerlessResponse {
  constructor() : super() {
    super.setStatusCode(201)
    super.setBody("Created")
  }

  constructor(request: ServerlessHttpRequest?) : super(request!!) {
    super.setStatusCode(201)
    super.setBody("Created")
  }
}
