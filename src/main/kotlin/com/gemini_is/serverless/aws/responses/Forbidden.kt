package com.gemini_is.serverless.aws.responses

import com.gemini_is.serverless.aws.ServerlessHttpRequest
import com.gemini_is.serverless.aws.ServerlessResponse

class Forbidden : ServerlessResponse {
  constructor() : super() {
    super.setStatusCode(403)
    super.setBody("Forbidden")
  }

  constructor(request: ServerlessHttpRequest?) : super(request!!) {
    super.setStatusCode(403)
    super.setBody("Forbidden")
  }
}
